import React, { Component } from 'react';
import { connect } from 'react-redux';
import { action } from '../actions/action';
import { bindActionCreators } from 'redux';

class FetchData extends Component {
	constructor(props) {
		super(props);
		this.state = {
			data: null
		};
	}
	componentWillMount() {
		this.props.action();
	}
	// componentDidMount() {
	// 	fetch('https://jsonplaceholder.typicode.com/posts/')
	// 		.then((response) => response.json())
	// 		.then((json) => this.setState({ data: json }));
	// }
	render() {
		// console.log(this.props.data, 'render');
		return (
			<React.Fragment>
				{this.props.data ? (
					this.props.data.map((item, index) => (
						<div key={index}>
							<h1>{item.title}</h1>
							<p>{item.body}</p>
						</div>
					))
				) : null}
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state) => {
	// console.log(state, 45674);
	return {
		data: state.data.items
	};
};

const matchDispatchToProps = (dispatch) => {
	return {
		action: () => dispatch(action())
	};
};
// const matchDispatchToProps = (dispatch) => {
// 	return bindActionCreators({ action: action }, dispatch);
// };

export default connect(mapStateToProps, matchDispatchToProps)(FetchData);
