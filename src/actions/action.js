export const action = (value) => (dispatch) => {
	fetch('https://jsonplaceholder.typicode.com/posts/')
		.then((res) => res.json())
		.then((data) => {
			dispatch({
				type: 'FETCH_DATA',
				payload: data
			});
		})
		.catch((err) => console.log(err));
};
