import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import { Provider } from 'react-redux';
import FetchData from './components/FetchData';
import store from './store';

ReactDOM.render(
	<Provider store={store}>
		<FetchData />
	</Provider>,
	document.getElementById('root')
);
