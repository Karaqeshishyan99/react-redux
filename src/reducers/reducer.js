const initialState = {
	items: [],
	item: {}
};
export default function(state = initialState, action) {
	switch (action.type) {
		case 'FETCH_DATA':
			// console.log(state, 'state');
			return {
				...state,
				items: action.payload
			};
		default:
			return state;
	}
}
// setInterval(() => {
// 	console.log(initialState, 'initialState');
// }, 3000);
